# Exploring Direct Convolution Performance on the Gemmini Accelerator

This repository contains the source code and instructions to reproduce the experiments in the paper **Exploring Direct Convolution Performance on the Gemmini Accelerator**.
```
git clone https://gitlab.com/cravieira/wscad2020.git
```

## Dependencies

```
sudo apt install -y parallel python3 pip3
pip3 install matplotlib numpy
```

This repository depends on two other projects: [Chipyard](https://github.com/ucb-bar/chipyard) to generate custom RISC-V SoCs and [PCM](https://github.com/opcm/pcm) acquire energy data from the processor. You can find Chipyard requirements [here](https://chipyard.readthedocs.io/en/latest/Chipyard-Basics/Initial-Repo-Setup.html#initial-repository-setup). To build everything, just run the commands below, which are compiled in RUNME.sh. 

```
#!/bin/bash

set -e

# RISC-V
cd riscv
# Download required Chipyard submodules.
./init-chipyard.sh
# Build esp-tools toolchain. Rerunning this script will rebuild the
# toolchain, be aware.
./build-toolchain.sh
# Build Gemmini system used (GemminiRocketConfig).
./build-chipyard.sh
# Build RISC-V benchmarks.
./conv-build.sh

cd ./..

# CPU
cd cpu
# Build pcm library.
./build-pcm.sh
# Build CPU binaries.
./conv-build.sh
```

## Reproducing the Experiments

For each platform, there is a test.sh script which will run the tests and store the results in a result folder. The `riscv/test.sh` relies on [GNU Parallel](https://www.gnu.org/software/parallel/) to speed up the simulations by running each simulation in parallel jobs. You must run `cpu/test.sh` with sudo to enable PCM utilization. After running both scripts, use `plots.py` to create the plots.