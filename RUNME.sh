#!/bin/bash

set -e

# RISC-V
cd riscv
# Download required Chipyard submodules.
./init-chipyard.sh
# Build esp-tools toolchain. Rerunning this script will rebuild the
# toolchain, be aware.
./build-toolchain.sh
# Build Gemmini system used (GemminiRocketConfig).
./build-chipyard.sh
# Build RISC-V benchmarks.
./conv-build.sh

cd ./..

# CPU
cd cpu
# Build pcm library.
./build-pcm.sh
# Build CPU binaries.
./conv-build.sh

echo "All files built successfully."
