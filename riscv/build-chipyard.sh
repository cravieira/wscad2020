#!/bin/bash

set -ex

cd chipyard
source env.sh
cd sims/verilator
make CONFIG=GemminiRocketConfig
