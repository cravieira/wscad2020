#!/bin/bash

set -ex

source chipyard/env.sh

mkdir -p results
find build/bench/conv-def-b/* | sort -Vr | parallel -j$(nproc) --halt now,fail=1 "echo {};simulator-chipyard-GemminiRocketConfig {}" &>> results/conv-def-b.txt
find build/bench/conv-def-i/* | sort -Vr | parallel -j$(nproc) --halt now,fail=1 "echo {};simulator-chipyard-GemminiRocketConfig {}" &>> results/conv-def-in-dim.txt
