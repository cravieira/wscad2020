#!/bin/bash

set -ex

git submodule update --init chipyard

cd chipyard

git submodule update --init --recursive generators/rocket-chip
git submodule update --init --recursive generators/boom
git submodule update --init --recursive generators/testchipip
git submodule update --init --recursive generators/gemmini
git submodule update --init --recursive generators/hwacha
git submodule update --init --recursive generators/sifive-blocks
git submodule update --init --recursive generators/sifive-cache
git submodule update --init --recursive generators/tracegen
git submodule update --init --recursive generators/icenet
git submodule update --init generators/sha3
git submodule update --init generators/ariane
git submodule update --init generators/nvdla

git submodule update --init sims/firesim

git submodule update --init --recursive toolchains/esp-tools/riscv-gnu-toolchain
git submodule update --init --recursive toolchains/esp-tools/riscv-isa-sim
git submodule update --init --recursive toolchains/esp-tools/riscv-pk
#git submodule update --init --recursive toolchains/esp-tools/riscv-tests
#git submodule update --init --recursive toolchains/esp-tools/esp-tests

git submodule update --init --recursive tools/barstools
git submodule update --init --recursive tools/chisel3
git submodule update --init --recursive tools/chisel-testers
git submodule update --init --recursive tools/dsptools
git submodule update --init --recursive tools/firrtl
git submodule update --init --recursive tools/firrtl-interpreter
git submodule update --init --recursive tools/treadle
git submodule update --init --recursive tools/DRAMSim2
